#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Becky'

# date: 6/6/19
#
# This script is to test the json input feature for the updatePM webservice (40 percent project)
# when using json input, the webservice only accepts one SP and runs on clarityint
# the database table will be update for FD, AP and SP with new PM and status_comments
#

import json
import requests
from Utils.util_jsonInput import jsonInput

class upDatePM_webservice_test():
    def __init__(self):
        self.errorCnt = 0
        self.printOn = True

    # -------------------------------------------------------------------------------------------
    # setURL
    # build the URL to fix the request,
    def setURL(self):
        requestURL = 'http://localhost:8000/itssupp/update-pm/'
        print("url = " + requestURL)
        return requestURL

    # -------------------------------------------------------------------------------------------
    # spSubmissionPost
    #
    # inputs: requestURL
    def spSubmissionPost(self,requestURL,mySubmissionAttribute):

        mydata = json.dumps(mySubmissionAttribute)
        headers = {"Content-Type": "application/json", 'data':mydata}
        # Call REST API
        response = requests.post(requestURL, data=mydata, headers=headers)  #POST  request
        status = response.status_code
        if status != 200:
            print("*** Error, Unexpected Returned Status = ", status)
            self.errorCnt +=  1
            prettyJson = json.loads(response.text)
            print("WS request status = " + str(status))
            print("response:" + json.dumps(prettyJson, indent=4, sort_keys=True))
        else:
            # Print Response
            prettyJson = json.loads(response.text)
            if self.printOn:
                print (json.dumps(prettyJson, indent=4, sort_keys=True))
            return prettyJson



    # -------------------------------------------------------------------------------------------
    # doTests
    #
    def doTests(self):
        url = self.setURL()
        print("")
        print("--------update PM tests ---------")
        # set up the test with desired sp id and server
        if self.printOn:
            print("url = " + url)

        #self.updatePM_call(url)  # response in dictionary form

        requestJson = jsonInput().updatePMtoRJR  #  change  json for different test
        print("the request is:")
        print(json.dumps(requestJson, indent=4, sort_keys=True))
        # submit post
        print("the response is:")
        self.spSubmissionPost(url, requestJson)

        return self.errorCnt


# -------------------------------------------------------------------------------------------
# start here
# run tests

myTest = upDatePM_webservice_test()
errs = myTest.doTests()

print("")
print("---Number of Errors Found = " + str(errs) + " ---")
